
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path')
  , google = require('googleapis')
  , Q = require('q')
  , _ = require('underscore');


var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);
app.get('/users', user.list);

var googleAuth = {
  key: 'AIzaSyC1I7PEkeES4Xyf5NgRz4TpMWnFbV0Wkdg',
  cx: '011051641233120606156:0yqu7zoh45s'
};

var customsearch = google.customsearch('v1');

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
  
  //this is the query...
  whichIsIt("Angelina Jolie",["brunette","blonde","redhead","actress"]).then(function(res) {
	  console.log(res.it + " is " + res.is);
  });
});

function totalResultsFromSearch(searchCombo) {
	var deferred = Q.defer();
	var totalResults = 0;
	Q.allSettled([
/* 		resultsFromSearch("'is " + searchCombo.it + " " + searchCombo.is + "?'"), */
		resultsFromSearch("'" + searchCombo.it + " is a " + searchCombo.is + "'"),
		resultsFromSearch(searchCombo.it + " " + searchCombo.is)
		]).done(function(result) {
			var totalNum = 0;
			_.each(result, function (res) {
				console.log('value: ' + res.value);
				totalNum+=res.value;
			});
			deferred.resolve(totalNum);
		});
/* 	totalResults+= resultsFromSearch("'is " + searchCombo.it + " " + searchCombo.is + "?'"); */
/* 	totalResults+= resultsFromSearch("'" + searchCombo.it + " is " +  + searchCombo.is + "'"); */
	return deferred.promise;
}

function whichIsIt(It,things){
	var deferred = Q.defer();
	Q.allSettled(_.map(things,function(thing) {
/* 		return resultsFromSearch(It + " " + thing) */
		return totalResultsFromSearch({it: It, is: thing})
		})).done(function(res) {
			var answer = _.max(res,function(result) {
				return result.value;
			});
		var answerIdx = res.indexOf(answer);
		deferred.resolve({it: It, is: things[answerIdx]});
	});
	return deferred.promise;
}

function resultsFromSearch(searchTerms){
	var deferred = Q.defer();
	customsearch.cse.list({ cx: googleAuth.cx, q: searchTerms, auth: googleAuth.key }, function(err, resp) {
	  if (err) {
	    console.log('An error occured', err);
	    return;
	  }
	  if (resp.items && resp.items.length > 0) {
	  		console.log("search: " + searchTerms + " and results: " + resp.searchInformation.totalResults);
		  deferred.resolve(resp.searchInformation.totalResults);
	  }  
	});
	return deferred.promise;
}